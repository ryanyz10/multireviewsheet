\documentclass{article}
\usepackage{amsmath}
\usepackage{charter}
\usepackage{parskip}
\usepackage{microtype}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{placeins}
\begin{document}
\setlength{\abovedisplayskip}{1pt}
\setlength{\belowdisplayskip}{1pt}
\makeatother
\title{Multivariable Calculus Review}
\author{Ryan Zhou \and Lewis Jones \and Allan Clarke \and Parker Hamlin}
\maketitle
\clearpage
\section*{13.9: Applications of Extrema of Functions of Two Variables}
\subsection*{Information}
We call a point $(x_0, y_0)$ a critical point when $f_x(x_0, y_0) = f_y(x_0, y_0) = 0$ or either $f_x(x_0, y_0)$ or $f_y(x_0, y_0)$ is undefined.

Relative extrema \textbf{ONLY} occur at critical points.

A useful test is the second partials test. Let $f$ have continuous second partial derivatives containing a point $(a, b)$ such that $f_x(x_0, y_0) = f_y(x_0, y_0) = 0$. Define $d$ as
\begin{center}
$$d = f_{xx}(a, b)f_{yy}(a, b) - [f_{xy}(a, b)]^2.$$
\end{center}
If $d > 0$ and $f_{xx}(a, b) > 0$, then $f$ has a relative min at $(a, b)$.

If $d > 0$ and $f_{xx}(a, b) < 0$, then $f$ has a relative max at $(a, b)$.

If $d < 0$, there is a saddle point at $(a, b, f(a, b)$.

If $d = 0$, we cannot conclude anything.

With this information, we are ready to move to applications. The solution process will be reviewed through the examples.

\subsection*{Examples}
\textbf{Example 1}: A retail outlet sells two types of riding lawn mowers, the prices of which are $p_1$ and $p_2$. Find $p_1$ and $p_2$ so as to maximize total revenue, where $R = 515p_1 + 805p_2 + 1.5p_1p_2 - 1.5p_1^2 - p_2^2$.

\textbf{Solution}: We first take the partial derivatives:
\begin{center}
$$R_{p_1} = 515 + 1.5p_2 - 3p_1$$ and $$R_{p_2} = 805 + 1.5p_1 - 2p_2$$
\end{center}
Setting these both to $0$ gives us the system of equations
\begin{center}
$$3p_1 - 1.5p_2 = 515$$
$$2p_2 - 1.5p_1 = 805$$
\end{center}
If we solve this, we find that $p_1 = \frac{1790}{3}$ and $p_2 = 850$.

Also, $R_{p_1p_1} = -3$, $R_{p_2p_2} = -2$ and $R_{p_1p_2} = 1.5$. Then $d = (-3)(-2) - (1.5)^2 = 3.75 > 0$. Since $d > 0$ and $R_{p_1p_1} < 0$, there is a relative max when $p_1 = \$596.67$ and $p_2 = \$850$.

\subsection*{Real Life Application}
As the section title might have given away, there are many applications for finding extrema. An obvious one is a business that wants to estimate the price and quantity of each of it's products it needs to sell in order to maximize revenue. Another one is minimizing the distance of a central warehouse from several stores.

\section*{13.10: Lagrange Multipliers}
\subsection*{Information}
We start with a quick review about \textbf{Gradients}. Given a function $f(x, y)$, the gradient is the vector
\begin{center}
$$\nabla f(x, y) = f_x(x, y)\textbf{i} + f_y(x, y)\textbf{j}.$$
\end{center}
It gives the direction of fastest increase at each point of the function.

We then state an important theorem.

\textbf{Lagrange's Theorem}: Let $f$ and $g$ have continuous first partial derivatives such that $f$ has an extremum at a point $(x_0, y_0)$ on the smooth constraint curve $g(x, y) = c$. If $\nabla g(x_0, y_0) \neq 0$, then there is a real number $\lambda$ such that 
\begin{center}
$$\nabla f(x_0, y_0) = \lambda\nabla g(x_0, y_0).$$
\end{center} 
%https://en.wikipedia.org/wiki/Lagrange_multiplier
\begin{center}
	\begin{figure}[!htb]
		\includegraphics[width=\linewidth]{lagrange.png}
	\end{figure}
\end{center}
In the image above, the gradient functions are represented by the $2$ black ellipses. The blue curve is the function $f$ that we want to maximize, and the red line is the constraint function $g$. In order to maximize, we want to find the point when $f$ and $g$ are tangent to each other. This happens when the gradients are parallel. Since the gradient is a vector, and vectors are parallel only when they are scalar multiples of each other, we want to find the $\lambda$ such that $\nabla f(x, y) = \lambda\nabla g(x, y)$. The scalar $\lambda$ is known as the Lagrange Multiplier. 

To find the extremum, we want to solve $\nabla f(x, y) = \lambda\nabla g(x, y)$. We can do this by solving the system
\begin{center}
$$f_x(x, y) = \lambda g_x(x, y)$$
$$f_y(x, y) = \lambda g_y(x, y)$$
$$g(x, y) = c.$$
\end{center}
We can extend this to higher dimensions by solving more equations.
\subsection*{Examples}
\textbf{Example 1}: Find all critical points of f on the given surface, then determine the maxima and minima by evaluating f at the critical values. 

$f(x, y, z) = x + y + 2z$ on the surface $x^2 + y^2 + z^2 = 3$.

We have $f(x, y, z) = x + y + 2z$ and $g(x, y, z) = x^2 + y^2 + z^2$, so $\nabla f = \textbf{i} + \textbf{j} + 2\textbf{k}$ and $\nabla g = 2\textbf{x} + 2\textbf{y} + 2\textbf{z}$. Then split these into four equations:
\begin{center}
$$1 = 2\lambda x$$
$$1 = 2\lambda y$$
$$2 = 2\lambda z$$
$$x^2 + y^2 + z^2 = 3$$
\end{center}

We then solve these for the $x$, $y$, and $z$ terms: 
\begin{center}
$$x = \frac{1}{2\lambda}$$
$$y = \frac{1}{2\lambda}$$
$$z = \frac{1}{\lambda}.$$
\end{center}

Then, plug these into the fourth equation (the constraint), and solve
\begin{center}
$$\frac{1}{4\lambda^2} + \frac{1}{4\lambda^2} + \frac{1}{\lambda^2} = 3.$$
\end{center} 
Thus 
\begin{center}
$$\lambda = \pm\frac{1}{\sqrt{2}}.$$
\end{center}

Plug the $\lambda$ value into the $x$, $y$, and $z$ equations. This gives the critical points: $(\frac{\sqrt{2}}{2}, \frac{\sqrt{2}}{2}, \sqrt{2})$ and $(-\frac{\sqrt{2}}{2}, -\frac{\sqrt{2}}{2}, -\sqrt{2})$.

Finally, plug these critical points into the original equation, $f(x, y, z) = x + y + 2z$.
\begin{center}
$f(\frac{\sqrt{2}}{2}, \frac{\sqrt{2}}{2}, \sqrt{2}) = 3\sqrt{2}$ and $f(-\frac{\sqrt{2}}{2}, -\frac{\sqrt{2}}{2}, -\sqrt{2}) = -3\sqrt{2}$.
\end{center}

So $(\frac{\sqrt{2}}{2}, \frac{\sqrt{2}}{2}, \sqrt{2})$ is the maximum and $(-\frac{\sqrt{2}}{2}, -\frac{\sqrt{2}}{2}, -\sqrt{2})$ is the minimum.
\subsection*{Real World Application}
Being able to find the maxima and minima of equations under constraints is useful in a variety of applications. Lagrange multipliers can be used in physics, to find the minimization of an energy particle, or even in economics to maximize profit under certain conditions. For example, Coca Cola could use Lagrange Multipliers to maximize the volume of soda a can could hold, while minimizing size and material used. 

\section*{14.1: Iterated Integrals and Area in the Plane}
\subsection*{Information}
In this section, we will deal with iterated integrals, which allow us to integrate over 2 or more variables. They are written in the form 
\begin{center}
$$\int\limits_a^b\int\limits_{g_1(x)}^{g_2(x)}f(x,y)\,dy\,dx$$ or $$\int\limits_c^d\int\limits_{h_1(y)}^{h_2(y)}f(x,y)\,dx\,dy$$
\end{center}
where $g(x)$ and $h(y)$ are the inside limits of integration and $a$, $b$, $c$, and $d$ are the outside limits of integration. Both determine the region of integration, denoted $R$.

We can thus use iterated integrals to find the area of this plane region $R$. Let a region $R$ be vertically simple if the outer limits of integration are vertical lines, and similarly let $R$ be horizontally simple if the outer limits of integration are horizontal lines. 

If $R$ is vertically simple, we can find the area bounded by $a$, $b$ and $g_1$, $g_2$ using the iterated integral
\begin{center}
$$\int\limits_a^b\int\limits_{g_1(x)}^{g_2(x)}\,dy\,dx$$
\end{center}
and if it is horizontally simple, we can find the area bounded by $c$, $d$ and $h_1$, $h_2$ using the iterated integral
\begin{center}
$$\int\limits_c^d\int\limits_{h_1(x)}^{h_2(x)}\,dx\,dy.$$
\end{center}
Be sure to note the order of $dy$ and $dx$! 

\subsection*{Examples}
\textbf{Example 1}: Evaluate the iterated integral
\begin{center}
$$\int\limits_1^4\int\limits_1^{\sqrt{x}}2ye^{-x}\,dy\,dx$$
\end{center}
\textbf{Solution}:
This is one of the simpler problems. We just evaluate the integral:
\begin{center}
$$\int\limits_1^4\int\limits_1^{\sqrt{x}}2ye^{-x}\,dy\,dx$$
$$ = \int\limits_1^4e^{-x}\int\limits_1^{\sqrt{x}}2y\,dy\,dx$$
$$ = \int\limits_1^4e^{-x}[y^2]_1^{\sqrt{x}}\,dx$$
$$ = \int\limits_1^4e^{-x}(x - 1)\,dx$$
$$ = [-e^{-x}(x - 1) + \int e^{-x}dx]_1^4$$
$$ = [(-e^{-x}(x)]_1^4 = -4e^{-4} + e^{-1}.$$
\end{center}

\textbf{Example 2}: 
Find the area of the region bounded below by $y = x + 2$ and above by $y = 4 - x^2$

\textbf{Solution}:
We start with a diagram:
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.3]{14-1Ex2.png}
\end{figure}
\FloatBarrier
The first thing we do is look for the intersection points of these two equations. 
\begin{center}
$$x + 2 = 4 - x^2$$
$$x^2 + x - 2 = 0$$
$$(x + 2)(x - 1) = 0$$
$$x = -2, 1$$
\end{center}
Since the vertical boundaries are straight, the area is vertically simple. The $x$ values are then our outer limits. Our lower inner limit is $y = x + 2$ and our upper inner limit is $y = 4 - x^2$. The area is then
\begin{center}
$$\int\limits_{-2}^1\int\limits_{x + 2}^{4 - x^2}\,dy\,dx$$
\end{center}
Solving is simple from this point. After integrating, we get that the area is $\frac{9}{2}$.
\subsection*{Real World Application}
We can use iterated integrals to calculate the area of the surface of a pool to find out how many tiles are needed to completely cover the bottom of the pool.

\section*{14.2: Double Integrals and Volume}
\subsection*{Information}
\textbf{Definition of Double Integral}: If $f$ is defined on a closed, bounded region $R$ in the $xy$-plane, then the double integral of $f$ over $R$ is given by 
\begin{center}
$$\iint_R f(x, y)\,dA = \lim_{||\Delta||\to\infty}\sum\limits_{i = 1}^n f(x_i, y_i)\,\Delta A_i.$$
\end{center}
If the limit exists, then $f$ is integrable over $R$.

The primary purpose of double integrals is to find the volume contained under a surface $f$ and bound by a region $R$. We can find volume using the double integral with the formula
\begin{center}
$$V = \iint_R f(x, y)\,dA.$$
\end{center}
However, in this form, we are unable to evaluate the double integral. This is where Fubini's Theorem comes in.

\textbf{Fubini's Theorem}: Let $f$ be defined on a plane region $R$. If $R$ is vertically simple then
\begin{center}
$$\iint_R f(x, y)\,dA = \int\limits_a^b\int\limits_{g_1(x)}^{g_2(x)} f(x, y)\,dy\,dx.$$
\end{center}
If $R$ is horizontally simple then
\begin{center}
$$\iint_R f(x, y)\,dA = \int\limits_c^d\int\limits_{h_1(y)}^{h_2(y)} f(x, y)\,dx\,dy.$$
\end{center}

\subsection*{Examples}
\textbf{Example 1}: Find the volume of the figure bound by the plane $2x + 3y + 4z = 12$ and the three coordinate planes.

\textbf{Solution}:
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.6]{14-2Ex1.png}
\end{figure}
\FloatBarrier
This is a pretty simple problem. We first calculate where the plane intersects the axes. Plugging in $0$ for $y$ and $z$, we see that the plane intersects the $x$ axis at $(6, 0, 0)$. Using a similar process, we find the y-intercept is $(0, 4, 0)$ and the z-intercept is $(0, 0, 3)$.

\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.6]{14-2Ex1-2.png}
\end{figure}
\FloatBarrier
If we let $R$ be the area in the $xy$-plane, we can let the outer bounds be $x = 0$ and $x = 6$ and the inner bounds be $y = 0$ and $y = -\frac{2}{3}x+ 4$. Our integral is then
\begin{center}
$$V = \iint_R f(x, y)\,dA = \int\limits_{0}^{6}\int\limits_{0}^{-\frac{2}{3}x + 4} \frac{12 - 2x - 3y}{4}\,dy\,dx.$$
\end{center}
We expand and evaluate to find the volume is $12$. We can check this answer: the volume of a triangular pyramid is 
\begin{center}
$$\frac{1}{6}lwh = \frac{1}{6}(6)(4)(3) = 12.$$
\end{center}

\textbf{Example 2}: Find the volume of the region bound by $z = 1 - xy$, $y = 1$, $y = x$ and the $xy$-plane.
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.8]{14-2Ex2-1.png}
\end{figure}
\FloatBarrier
\textbf{Solution}: We first sketch a diagram of $R$:
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.5]{14-2Ex2-2.png}
\end{figure}
\FloatBarrier

It should be fairly easy to write out our integral:
\begin{center}
$$V = \int\limits_0^1\int\limits_0^y (1 - xy)\,dx\,dy$$
\end{center}
As we've done thousands of times before, we evaluate this integral.
\begin{center}
$$V = \int\limits_0^1\int\limits_0^y (1 - xy)\,dx\,dy$$
$$ = \int\limits_0^1[x - \frac{1}{2}x^2y]_0^y\,dy$$
$$ = \int\limits_0^1(y - \frac{1}{2}y^3)\,dy$$
$$ = [\frac{1}{2}y^2 - \frac{1}{8}y^4]_0^1$$
$$ = \frac{1}{2} - \frac{1}{8} = \frac{3}{8}.$$
\end{center}

\subsection*{Real World Application}
Double Integrals can be used to find the volume of almost any object, as long as the equation is known. For example, it could be used to find how much water can be fit in cups of various shapes and compare the volume contained in each cup.

\section*{14.3 Change of Variables: Polar Coordinates}
\subsection*{Information}
Occasionally, we get double integrals that are much easier to solve in polar coordinates. A quick recap of polar coordinates:
%http://www.shelovesmath.com/trigonometry/polar-graphs/
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.35]{polar.png}
\end{figure}
\FloatBarrier
In polar coordinates, each point is represented by a radius, $r$, from the origin, and an angle, $\theta$, from $0^\circ$. We can convert between rectangular and polar coordinates using the following equations
\begin{center}
$$x^2 + y^2 = r^2$$
$$x = r\cos\theta$$
$$y = r\sin\theta.$$
\end{center}
In order to change a rectangular double integral into a polar iterated integral, we use the formula
\begin{center}
$$\iint_R f(x,y)\,dA = \int\limits_{\alpha}^{\beta}\int\limits_{g_1(\theta)}^{g_2(\theta)} f(r\cos\theta, r\sin\theta)\,r\,dr\,d\theta$$
\end{center}
Solving problems is pretty much the same, so we will get right to examples. 

\subsection*{Examples}
\textbf{Example 1}: Evaluate the double integral
\begin{center}
$$\int\limits_0^{2\pi}\int\limits_0^6 3r^2\sin\theta\,dr\,d\theta$$
\end{center}

\textbf{Solution}: 
A simple example to get started.
\begin{center}
$$\int\limits_0^{2\pi}\int\limits_0^6 3r^2\sin\theta\,dr\,d\theta$$
$$ = \int\limits_0^{2\pi}\sin\theta\int\limits_0^6 3r^2\,dr\,d\theta$$
$$ = \int\limits_0^{2\pi}\sin\theta [r^3]_0^6\,d\theta$$
$$ = 216\int\limits_0^{2\pi}\sin\theta\,d\theta$$
$$ = 216[-\cos\theta]_0^{2\pi} = 0.$$ {\tiny (Surprise!)}
\end{center}
Pretty simple and familiar, right?

\textbf{Example 2}: Convert to polar coordinates and solve
\begin{center}
$$\int\limits_0^3\int\limits_0^{\sqrt{9 - x^2}} (x^2 + y^2)^\frac{3}{2}\,dy\,dx.$$
\end{center}
\textbf{Solution}: If we sketch the equation given by the bounds (do it by yourself this time) we see that the $R$ is a quarter circle in the first quadrant with radius $3$.
Then $\theta$ ranges from $0$ to $\frac{\pi}{2}$ and $r$ ranges from $0$ to $3$. Converting to rectangular:
\begin{center}
$$\int\limits_0^{\frac{\pi}{2}}\int\limits_0^3 (r^2)^\frac{3}{2}\,r\,dr\,d\theta$$
$$ = \int\limits_0^{\frac{\pi}{2}} \int\limits_0^3 r^4\,dr\,d\theta$$
$$ = \int\limits_0^{\frac{\pi}{2}} [\frac{1}{5}r^5]_0^3\,d\theta$$
$$ = \frac{243}{5}\int\limits_0^{\frac{\pi}{2}}\,d\theta = \frac{243\pi}{10}$$
\end{center}

\textbf{Example 3}: Find the area of the following:
\begin{figure}[!htb]
    \centering
    \includegraphics[scale=0.5]{14-3Ex3.png}
\end{figure}
\FloatBarrier

\textbf{Solution}: We notice that each of the ``petals'' is the same, so we can find the area of one ``petal'' and multiply by $3$. The first petal starts and ends when $r = 0$, so we can solve to get $\theta$. If $0 = 2\sin3\theta$, then $\sin3\theta = 0$ and $\theta = 0, \frac{\pi}{3}, \frac{2\pi}{3} \cdots$. Since we are after the first petal, we can let the bounds for $\theta$ be $0$ and $\frac{\pi}{3}$.

We can now write our integral:
\begin{center}
$$A = 3\int\limits_0^{\frac{\pi}{3}} \int\limits_0^{2\sin3\theta} r\,dr\,d\theta$$
$$ = 3\int\limits_0^{\frac{\pi}{3}} [\frac{1}{2}r^2]_0^{2\sin3\theta}\,d\theta$$
$$ = 3\int\limits_0^{\frac{\pi}{3}} 2\sin^2 3\theta\,d\theta$$
$$ = 3\int\limits_0^{\frac{\pi}{3}} (1 - \cos6\theta)\,d\theta$$
$$ = 3[\theta - \frac{1}{6}\sin6\theta]_0^{\frac{\pi}{3}} = \pi$$
\end{center}
\clearpage
\begin{thebibliography}{9}
\bibitem{harvardmath}
Harvard Math 21A Lagrange Multipliers,\\
\texttt{http://www.math.harvard.edu/archive/21a\_spring\_09/PDF/11-08-Lagrange-Multipliers.pdf}
\bibitem{wolfram}
Wolfram Mathworld-Lagrange Multipliers,\\
\texttt{http://mathworld.wolfram.com/LagrangeMultiplier.html}
\bibitem{wikipedia} 
Wikipedia-Lagrange Multipliers,\\
\texttt{https://en.wikipedia.org/wiki/Lagrange\_multiplier}
\bibitem{shelovesmath}  
Polar Graphs,\\
\texttt{http://www.shelovesmath.com/trigonometry/polar-graphs/}
\end{thebibliography}
\end{document}