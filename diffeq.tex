\documentclass{article}
\usepackage{amsmath}
\usepackage{charter}
\usepackage{parskip}
\usepackage{microtype}
\usepackage[margin=1in]{geometry}
\usepackage{graphicx}
\graphicspath{{images/}}
\usepackage{placeins}
\begin{document}
\setlength{\abovedisplayskip}{1pt}
\setlength{\belowdisplayskip}{1pt}
\title{Differential Equation Review}
\author{Ryan Zhou \and Lewis Jones \and Allan Clarke \and Parker Hamlin}
\maketitle
\clearpage
\section*{1.5: Differential Form of DiffEQ}
\subsection*{Information}
General form of solution: $F(x, y(x)) = C$

If we take the partial derivative with respect to x of the general form, we get:
\begin{center}
$$\frac{\partial F}{\partial x} + \frac{\partial F}{\partial y}\cdot\frac{dy}{dx} = 0.$$
\end{center}
We can rewrite this as 
\begin{center}
$$M(x,y) + N(x,y)\frac{dy}{dx} = 0.$$
\end{center}
where 
\begin{center}
$$M(x, y) = F_x(x, y)$$ and $$N(x, y) = F_y(x, y).$$
\end{center}
Multiplying both sides by $dx$ yields the differential form:
\begin{center}
$$M(x,y)\, + N(x,y)\,dy = 0$$
\end{center}

If $F(x, y)$ is a function such that $M = F_x$ and $N = F_y$, then we call it an exact differential equation. In other words, if $Mdy = Ndx$, the equation is exact.

To solve, we integrate $M$ with respect to $x$ and $N$ with respect to $y$. The solution will be the function $F(x, y)$ that has all the terms from these two expression.
\subsection*{Examples}
\textbf{Example 1}: 
Prove the equation is exact, and solve:
\begin{center}
$(3x^2 + 2y^2)\,dx + (4xy + 6y^2)\,dy = 0$
\end{center}
\textbf{Solution}: 
In this equation we see that 
\begin{center}
$$M\,dy = (3x^2 + 2y^2)dy = 4y = (4xy + 6y^2)dx = N\,dx$$
\end{center}
so the equation is exact. 

Integrating $M$, we get
\begin{center}
$$\int M = \int (3x^2 + 2y^2)\,dx = x^3 + 2xy^2 + C$$
\end{center}
and integrating $N$ gives us
\begin{center}
$$\int N = \int (4xy + 6y^2)\,dy = 2xy^2 + 2y^3 + C$$
\end{center}
Thus, the solution is
\begin{center}
$$F = x^3 + 2xy^2 + 2y^3 + C.$$
\end{center}

\textbf{Example 2}:
Prove the equation is exact, and solve:
\begin{center}
$$(\frac{2x}{y} - \frac{3y^2}{x^4})\,dx + (\frac{2y}{x^3} - \frac{x^2}{y^2} + \frac{1}{\sqrt{y}})\,dy = 0$$
\end{center}
\textbf{Solution}:
This is merely a more complex equation than the above. The process is the same...

Taking the partial derivative of $M$ with respect to $y$... 
\begin{center}
$$M\,dy = \neg\frac{2x}{y^2} - \frac{6y}{x^4}$$
\end{center}
and $N$ with respect to $x$...
\begin{center}
$$N\,dx = \neg\frac{6y}{x^4} - \frac{2x}{y^2}$$
\end{center}
Since $Mdy = Ndx$, the equation is exact.

Integrating $M$:
\begin{center}
$$\int M = \int (\frac{2x}{y} - \frac{3y^2}{x^4})\,dx = \frac{x^2}{y} + \frac{y^2}{x^3} + C$$
\end{center}
and Integrating $N$:
\begin{center}
$$\int N = \int (\frac{2y}{x^3} - \frac{x^2}{y^2} + \frac{1}{\sqrt{y}})\,dy = \frac{y^2}{x^3} + \frac{x^2}{y} + 2\sqrt{y} + C$$
\end{center}

So our solution $F$ is 
\begin{center}
$$F = \frac{y^2}{x^3} + \frac{x^2}{y} + 2\sqrt{y} + C$$
\end{center}

\section*{Solving Separable Differential Equations}
Rarely, when we are fortunate, we will get a differential equation that is separable, meaning the terms containing $x$ can easily be separated from the terms containing $y$. We can then integrate both sides to get the solution.

\textbf{Example}: Solve the equation
\begin{center}
$$-y\,dx + x\,dy = 2y\,dx - dy$$
\end{center}
\textbf{Solution}:
\begin{center}
$$-y\,dx + x\,dy = 2y\,dx - dy$$
$$-3y\,dx + (x+1)\,dy = 0$$
$$(x+1)\,dy = 3y\,dx$$
$$\frac{1}{3y}\,dy = \frac{1}{x+1}\,dx$$
$$\int\frac{1}{3y}\,dy = \int\frac{1}{x+1}\,dx$$
$$\frac{1}{3}\ln y = \ln(x+1) + C$$
$$\ln y = 3\ln(x+1)+C$$
$$y = C(x + 1)^3$$
\end{center}

\section*{Solving Linear First Order Differential Equations}
If an equation is in the form 
\begin{center}
$$\frac{dy}{dx} + P(x)y = Q(x)$$
\end{center}
we call if a Linear First Order Equation. The integrating factor is 
\begin{center}
$$\rho(x) = e^{\int P(x)\,dx}.$$
\end{center}
The solution will be in the form
\begin{center}
$$y = e^{-\int P(x)\,dx}(\int Q(x)e^{\int P(x)\,dx}\,dx + C)$$
\end{center}
As we will see in \textbf{1.7: Solutions by Substitution cont.}, the linear first order form is a specific case of Bernoulli's Equation.

\section*{1.6: Solutions by Substitution}
\subsection*{Information}
We call a function $f$ homogeneous if 
\begin{center}
$$f(tx, ty) = t^af(x, y)$$
\end{center}
We can then define a homogeneous differential equation as 
\begin{center}
$$M(x, y)\,dx + N(x, y)\,dy = 0,$$
\end{center}
where $M$ and $N$ are homogeneous functions.

We can solve a homogeneous differential equation by the substitutions
\begin{center}
$$y = ux$$ or $$x = vy.$$
\end{center}
The substitution we choose depends on the complexity of $M$ and $N$: if $M$ is simple, use $x = vy$, and if $N$ is simple, use $y = ux$. 

This should make the equation separable, and solving becomes a trivial matter.

\subsection*{Examples} 
\textbf{Example 1}: 
Prove that the equation
\begin{center}
$$(x - y)\,dx + x\,dy = 0$$
\end{center}
is homogeneous, and solve.

\textbf{Solution}:
We first note that 
\begin{center}
$$M(x, y) = x - y$$ and $$N(x, y) = x.$$
\end{center}
Plugging in $tx$ and $ty$ for $x$ and $y$ in $M(x, y)$:
\begin{center}
$$M(tx, ty) = tx - ty = t(x - y),$$
\end{center}
so $M(x, y)$ is homogeneous. Similarly for $N$, 
\begin{center}
$$N(tx, ty) = tx$$
\end{center}
so $N(x, y)$ is also homogeneous. Since both $M$ and $N$ are homogeneous, the given equation is a homogeneous differential equation.

Between $M(x, y) = x - y$ and $N(x, y) = x$, we note that $N$ is the simpler function. Then we will use the substitution $y = ux$ and $dy = udx + xdu$. Plugging this in to the original equation,
\begin{center}
$$(x - ux)\,dx + x(u\,dx + x\,du) = 0$$
$$x\,dx - ux\,dx + ux\,dx + x^2\,du = 0$$
$$x\,dx + x^2\,du = 0$$
$$x^2\,du =  -x\,dx$$
$$du = -\frac{1}{x}\,dx$$
$$\int \,du = -\int\frac{1}{x}\,dx$$
$$u = -\ln x + C$$
\end{center}

We then substitute back for $u$:
\begin{center}
$$u = \frac{y}{x} = -\ln x + C$$
$$y = -x\ln x + Cx$$
\end{center}

\textbf{Example 2}: 
Prove that the equation
\begin{center}
$$xy^2\frac{dy}{dx} = y^3 - x$$
\end{center}
is not homogeneous.

\textbf{Solution}: This equation is not in a form that we are used to, so we start by multiplying both sides by $dx$.
\begin{center}
$$xy^2\,dy = (y^3 - x)\,dx$$
$$(x - y^3)\,dx + xy^2\,dy = 0$$
\end{center}
Then 
\begin{center}
$$M(x, y) = x - y^3$$ and $$N(x, y) = xy^2$$
\end{center}

Since 
\begin{center}
$$M(tx, ty) = tx - (ty)^3 = tx - t^3y^3$$
\end{center}

Since $M$ is not homogeneous, the equation is not homogeneous.

\section*{1.7: Solutions by Substitution cont.}
\subsection*{Information}
This section focuses on solving equations in the form
\begin{center}
$$\frac{dy}{dx} + P(x)y = f(x)y^n$$
\end{center}
where $n \geq 0$. This is also known as Bernoulli's Equation.

When $n = 0$, we are left with
\begin{center}
$$\frac{dy}{dx} + P(x)y = f(x)$$
\end{center}
which is a linear first order differential equation. 

When $n = 1$, the equation is separable and solving is simple.

When $n > 1$, we can use the substitution
\begin{center}
$$u = y^{1 - n}$$
\end{center}
which makes the equation either separable or a linear first order. We can solve using previous methods.
\subsection*{Examples}
\textbf{Example 1}:
Solve the Bernoulli's Equation
\begin{center}
$$\frac{dy}{dx} = y(xy^3 - 1).$$
\end{center}
\textbf{Solution}: This equation is not in the normal form of the Bernoulli's Equation, so we try to put it in that form. Expanding the left side:
\begin{center}
$$\frac{dy}{dx} = xy^4 - y$$
\end{center}
Rearranging terms gives us the desired form
\begin{center}
$$\frac{dy}{dx} + y = xy^4.$$
\end{center}
We can see that $P(x) = 1$, $f(x) = x$ and $n = 4$. Since $n > 1$, we will use the substitution method. 

Our desired substitution is 
\begin{center}
$$u = y^{1 - n} = y^{1 - 4} = y^{-3} = \frac{1}{y^3}$$
\end{center}
so 
\begin{center}
$$y = \frac{1}{u^{\frac{1}{3}}} = u^{-\frac{1}{3}}$$
\end{center}
and
\begin{center}
$$\frac{dy}{dx} = -\frac{1}{3}u^{-\frac{4}{3}}\frac{du}{dx}$$
\end{center}
Plugging this in...
\begin{center}
$$-\frac{1}{3}u^{-\frac{4}{3}}\frac{du}{dx} + u^{-\frac{1}{3}} = xu^{-\frac{4}{3}}.$$
\end{center}
If we divide by $u^{-\frac{4}{3}}$ we get
\begin{center}
$$-\frac{1}{3}\frac{du}{dx} + u = x$$ or $$\frac{du}{dx} - 3u = -3x$$
\end{center}
We note that this is now a linear first order equation. Thus, the solution is 
\begin{center}
$$u = e^{3\int dx}(\int(-3x)e^{-3\int dx}\,dx + C)$$
\end{center}
We now simplify this mess:
\begin{center}
$$u = e^{3x}(-3\int xe^{-3x}\,dx + C)$$
$$u = e^{3x}(xe^{-3x} + \frac{1}{3}e^{-3x} + C)$$
$$u = x + \frac{1}{3} + Ce^{3x}.$$
\end{center}
Finally, we plug back in for $y$:
\begin{center}
$$y^{-3} = x + \frac{1}{3} + Ce^{3x}$$
\end{center}
and our solution is finally
\begin{center}
$$y = \frac{1}{(x + \frac{1}{3} + Ce^{3x})^\frac{1}{3}}$$
\end{center}
\end{document}